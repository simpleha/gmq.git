package com.shuimutong.gmq.client.cache.bean;

/**
 * 缓存的对象
 * @ClassName:  CacheObj   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年11月24日 上午10:57:04     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
public class CacheObj {
	private String objId;
	private String parentObjId;
	private String content;
	public String getObjId() {
		return objId;
	}
	public void setObjId(String objId) {
		this.objId = objId;
	}
	public String getParentObjId() {
		return parentObjId;
	}
	public void setParentObjId(String parentObjId) {
		this.parentObjId = parentObjId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
}
