package com.shuimutong.gmq.client.util;

import java.util.HashMap;
import java.util.Map;

import com.shuimutong.gmq.client.bean.enums.CachePathTypeEnum;

public class UrlPackUtil {
	public static String getSendMessagePath() {
		return "/message/send";
	}
	public static String getGetMessagePath() {
		return "/message/get";
	}
	public static Map<CachePathTypeEnum, String> getCachePath() {
		Map<CachePathTypeEnum, String> map = new HashMap();
		map.put(CachePathTypeEnum.SET, "/cache/set");
		map.put(CachePathTypeEnum.SAVE_OR_UPDATE, "/cache/saveOrUpdate");
		map.put(CachePathTypeEnum.FIND_BY_OBJID, "/cache/findByObjId");
		map.put(CachePathTypeEnum.DEL_BY_OBJID, "/cache/delByObjId");
		map.put(CachePathTypeEnum.SET_WITH_EXPIRE_TIME, "/cache/setWithExpire");
		return map;
	}
}
