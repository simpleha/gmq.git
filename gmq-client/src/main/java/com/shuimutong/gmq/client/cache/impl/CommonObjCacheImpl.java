package com.shuimutong.gmq.client.cache.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.shuimutong.gmq.client.bean.HttpResponseBean;
import com.shuimutong.gmq.client.bean.ResponseDataVo;
import com.shuimutong.gmq.client.bean.enums.CachePathTypeEnum;
import com.shuimutong.gmq.client.cache.CommonObjCache;
import com.shuimutong.gmq.client.cache.bean.CacheObj;
import com.shuimutong.gmq.client.exception.ObjCacheException;
import com.shuimutong.gmq.client.util.HttpUtil;
import com.shuimutong.gmq.client.util.UrlPackUtil;

/**
 * 消息生产方
 * @ClassName:  GmqProducer   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年10月30日 下午9:37:58     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
public class CommonObjCacheImpl implements CommonObjCache {
	/**服务的地址**/
	private String serverUrl;
	/**缓存的全地址**/
	private Map<CachePathTypeEnum, String> cacheUrlMap = new HashMap();

	public CommonObjCacheImpl(String serverUrl) {
		this.serverUrl = serverUrl;
		Map<CachePathTypeEnum, String> pathMap = UrlPackUtil.getCachePath();
		pathMap.forEach((cacheType, path) -> {
			cacheUrlMap.put(cacheType, serverUrl + path);
		});
	}


	@Override
	public void save(String id, String content) throws ObjCacheException {
		Map<String, String> params = new HashMap();
		params.put("objId", id);
		params.put("content", content);
		try {
			HttpResponseBean responseBean = HttpUtil.get(cacheUrlMap.get(CachePathTypeEnum.SAVE_OR_UPDATE), params);
			boolean saveOk = false;
			if(responseBean != null && StringUtils.isNotBlank(responseBean.getBody())) {
				ResponseDataVo responseData = JSONObject.parseObject(responseBean.getBody(), ResponseDataVo.class);
				if(responseData != null && responseData.getState() == 200) {
					saveOk = true;
				}
			}
			if(!saveOk) {
				throw new RuntimeException("保存失败");
			}
		} catch (Exception e) {
			throw new ObjCacheException(e);
		}
	}


	@Override
	public CacheObj getById(String id) throws ObjCacheException {
		CacheObj savedObj = null;
		Map<String, String> params = new HashMap();
		params.put("objId", id);
		try {
			HttpResponseBean responseBean = HttpUtil.get(cacheUrlMap.get(CachePathTypeEnum.FIND_BY_OBJID), params);
			boolean state = false;
			if(responseBean != null && StringUtils.isNotBlank(responseBean.getBody())) {
				ResponseDataVo responseData = JSONObject.parseObject(responseBean.getBody(), ResponseDataVo.class);
				if(responseData != null && responseData.getState() == 200) {
					state = true;
					if(responseData.getData() != null) {
						savedObj = JSONObject.parseObject(responseData.getData().toString(), CacheObj.class);
					}
				}
			}
			if(!state) {
				throw new RuntimeException("获取失败");
			}
		} catch (Exception e) {
			throw new ObjCacheException(e);
		}
		return savedObj;
	}


	@Override
	public void delById(String id) throws ObjCacheException {
		Map<String, String> params = new HashMap();
		params.put("objId", id);
		try {
			HttpResponseBean responseBean = HttpUtil.get(cacheUrlMap.get(CachePathTypeEnum.DEL_BY_OBJID), params);
			boolean state = false;
			if(responseBean != null && StringUtils.isNotBlank(responseBean.getBody())) {
				ResponseDataVo responseData = JSONObject.parseObject(responseBean.getBody(), ResponseDataVo.class);
				if(responseData != null && responseData.getState() == 200) {
					state = true;
				}
			}
			if(!state) {
				throw new RuntimeException("删除失败");
			}
		} catch (Exception e) {
			throw new ObjCacheException(e);
		}
	}

	
}
