package com.shuimutong.gmq.client.bean.enums;

/**
 * 缓存路径类型枚举
 * @ClassName:  CachePathTypeEnum   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年11月24日 上午10:41:19     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
public enum CachePathTypeEnum {
	/**设置值，不带过期时间**/
	SET,
	/**新增或者更新**/
	SAVE_OR_UPDATE,
	/**通过objId查询**/
	FIND_BY_OBJID,
	/**根据Id删除**/
	DEL_BY_OBJID,
	/**设置带过期时间的值**/
	SET_WITH_EXPIRE_TIME,;
}
