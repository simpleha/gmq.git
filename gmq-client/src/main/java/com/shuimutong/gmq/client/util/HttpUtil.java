package com.shuimutong.gmq.client.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shuimutong.gmq.client.bean.HttpResponseBean;
import com.shuimutong.gutil.common.GUtilCommonUtil;

/**
 * http请求工具类
 * @ClassName: HttpUtil
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 水木桶
 * @date: 2019年10月29日 下午9:43:54
 * @Copyright: 2019 [水木桶] All rights reserved.
 */
public class HttpUtil {
	private final static Logger log = LoggerFactory.getLogger(HttpUtil.class);
	private static CloseableHttpClient HTTP_CLIENT = HttpClients.createMinimal();
	static {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				try {
					HTTP_CLIENT.close();
				} catch (IOException e) {
					log.error("HTTP_CLIENT-closeException", e);
				}
			}
		});
	}

	/**
	 * get请求
	 * 
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public static HttpResponseBean get(String url) throws IOException {
		HttpResponseBean responseBean = null;
		HttpGet httpGet = new HttpGet(url);
		CloseableHttpResponse res = HTTP_CLIENT.execute(httpGet);
		try {
			HttpEntity httpEntity = res.getEntity();
			String body = EntityUtils.toString(httpEntity);
			responseBean = new HttpResponseBean(res.getStatusLine(), body);
			EntityUtils.consume(httpEntity);
		} finally {
			res.close();
		}
		return responseBean;
	}
	
	/**
	 * 带参数的get请求
	 * @param url
	 * @param requsetParams
	 * @return
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public static HttpResponseBean get(String url, Map<String, String> requsetParams) throws IOException {
		HttpResponseBean responseBean = null;
		HttpGet httpGet;
		try {
			URIBuilder uriBuilder = new URIBuilder(url);
			if(!GUtilCommonUtil.checkListEmpty(requsetParams)) {
				List<NameValuePair> nvps = new ArrayList<NameValuePair>();
				requsetParams.forEach((k,v) -> {
					nvps.add(new BasicNameValuePair(k, v));
				});
				uriBuilder.setParameters(nvps);
			}
			httpGet = new HttpGet(uriBuilder.build());
		} catch (Exception e) {
			throw new IOException(e);
		}
		CloseableHttpResponse res = HTTP_CLIENT.execute(httpGet);
		try {
			HttpEntity httpEntity = res.getEntity();
			String body = EntityUtils.toString(httpEntity);
			responseBean = new HttpResponseBean(res.getStatusLine(), body);
			EntityUtils.consume(httpEntity);
		} finally {
			res.close();
		}
		return responseBean;
	}

	/**
	 * post请求
	 * @param url
	 * @param requsetParams
	 * @return
	 * @throws IOException
	 */
	public static HttpResponseBean post(String url, Map<String, String> requsetParams) throws IOException {
		HttpResponseBean responseBean = null;
		HttpPost httpPost = new HttpPost(url);
		if(!GUtilCommonUtil.checkListEmpty(requsetParams)) {
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			requsetParams.forEach((k,v) -> {
				nvps.add(new BasicNameValuePair(k, v));
			});
			httpPost.setEntity(new UrlEncodedFormEntity(nvps));
		}
		CloseableHttpResponse response = HTTP_CLIENT.execute(httpPost);
		try {
			HttpEntity httpEntity = response.getEntity();
			String body = EntityUtils.toString(httpEntity);
			responseBean = new HttpResponseBean(response.getStatusLine(), body);
			EntityUtils.consume(httpEntity);
		} finally {
			response.close();
		}
		return responseBean;
	}
}
