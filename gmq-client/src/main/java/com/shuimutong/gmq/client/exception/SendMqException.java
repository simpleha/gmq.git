package com.shuimutong.gmq.client.exception;

public class SendMqException extends Exception {

	public SendMqException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SendMqException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SendMqException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SendMqException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
