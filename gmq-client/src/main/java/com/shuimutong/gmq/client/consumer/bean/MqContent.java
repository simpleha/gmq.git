package com.shuimutong.gmq.client.consumer.bean;

public class MqContent {
	private long id;
	private String body;
	
	public MqContent(MqDto mq) {
		this.id = mq.getA();
		this.body = mq.getB();
	}
	public MqContent() {
		super();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}

}
