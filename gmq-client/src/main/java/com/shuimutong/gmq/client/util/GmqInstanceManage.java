package com.shuimutong.gmq.client.util;

import com.shuimutong.gmq.client.cache.CommonObjCache;
import com.shuimutong.gmq.client.cache.impl.CommonObjCacheImpl;
import com.shuimutong.gmq.client.consumer.GmqConsumer;
import com.shuimutong.gmq.client.producer.GmqProducer;

public class GmqInstanceManage {
	public static GmqProducer getGmqProducer(String gmqServerUrl) {
		return new GmqProducer(gmqServerUrl);
	}
	
	public static GmqConsumer getGmqConsumer(String gmqServerUrl) {
		return new GmqConsumer(gmqServerUrl);
	}
	
	public static CommonObjCache getCommonCache(String serverUrl) {
		return new CommonObjCacheImpl(serverUrl);
	}
}
