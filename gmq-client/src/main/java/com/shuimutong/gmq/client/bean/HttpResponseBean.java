package com.shuimutong.gmq.client.bean;

import org.apache.http.StatusLine;

public class HttpResponseBean {
	/**响应的状态码**/
	private int stateCode;
	/**响应说明**/
	private String reasonPhrase;
	/**返回内容**/
	private String body;
	
	public HttpResponseBean(StatusLine statusLine, String body) {
		if(statusLine != null) {
			this.stateCode = statusLine.getStatusCode();
			this.reasonPhrase = statusLine.getReasonPhrase();
		}
		this.body = body;
	}
	public HttpResponseBean() {
		super();
	}
	
	
	public int getStateCode() {
		return stateCode;
	}
	public void setStateCode(int stateCode) {
		this.stateCode = stateCode;
	}
	public String getReasonPhrase() {
		return reasonPhrase;
	}
	public void setReasonPhrase(String reasonPhrase) {
		this.reasonPhrase = reasonPhrase;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	@Override
	public String toString() {
		return "HttpResponseBean [stateCode=" + stateCode + ", reasonPhrase=" + reasonPhrase + ", body=" + body + "]";
	}
}
