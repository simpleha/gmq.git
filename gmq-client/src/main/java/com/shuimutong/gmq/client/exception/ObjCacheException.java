package com.shuimutong.gmq.client.exception;

public class ObjCacheException extends Exception {

	public ObjCacheException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ObjCacheException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ObjCacheException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ObjCacheException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
