package com.shuimutong.gmq.client.producer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.shuimutong.gmq.client.bean.HttpResponseBean;
import com.shuimutong.gmq.client.bean.SendMqResult;
import com.shuimutong.gmq.client.exception.SendMqException;
import com.shuimutong.gmq.client.util.HttpUtil;
import com.shuimutong.gmq.client.util.UrlPackUtil;

/**
 * 消息生产方
 * @ClassName:  GmqProducer   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年10月30日 下午9:37:58     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
public class GmqProducer {
	/**gmq服务的地址**/
	private String gmqServerUrl;
	/**gmq服务发送消息的全地址**/
	private String sendMqUrl;

	public GmqProducer(String gmqServerUrl) {
		this.gmqServerUrl = gmqServerUrl;
		this.sendMqUrl = gmqServerUrl + UrlPackUtil.getSendMessagePath();
	}
	
	public SendMqResult sendMq(String topic, String message) throws SendMqException {
		SendMqResult res = null;
		Map<String, String> params = new HashMap();
		params.put("topic", topic);
		params.put("message", message);
		try {
			HttpResponseBean responseBean = HttpUtil.post(sendMqUrl, params);
			if(responseBean != null) {
				res = new SendMqResult();
				res.setRes(responseBean.getBody());
			}
		} catch (IOException e) {
			throw new SendMqException(e);
		}
		return res;
	}
}
