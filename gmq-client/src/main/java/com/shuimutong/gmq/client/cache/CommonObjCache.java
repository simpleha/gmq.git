package com.shuimutong.gmq.client.cache;

import java.util.List;

import com.shuimutong.gmq.client.cache.bean.CacheObj;
import com.shuimutong.gmq.client.exception.ObjCacheException;

/**
 * 普通对象存储
 * @ClassName:  CommonObjCache   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年11月24日 上午10:58:24     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
public interface CommonObjCache {
	/**
	 * 添加包含过期时间的对象
	 * @param objId
	 * @param parentObjId
	 * @param content
	 * @param expireTimeSec
	 */
//	void addWithExpire(String objId, String parentObjId, String content, long expireTimeSec) throws ObjCacheException;
	/**
	 * 添加不含过期时间的对象
	 * @param objId
	 * @param parentObjId
	 * @param content
	 */
//	void add(String objId, String parentObjId, String content) throws ObjCacheException;
	/**
	 * 新增或者更新对象
	 * @param id
	 * @param content
	 */
	void save(String id, String content) throws ObjCacheException;
	/**
	 * 通过id获取
	 * @param id
	 * @return
	 */
	CacheObj getById(String id) throws ObjCacheException;
	/**
	 * 通过父objId获取列表
	 * @param parentObjId
	 * @return
	 */
//	List<CacheObj> listByParentObjId(String parentObjId) throws ObjCacheException;
	/**
	 * 根据id删除
	 * @param id
	 */
	void delById(String id) throws ObjCacheException;
}
