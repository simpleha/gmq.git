package com.shuimutong.gmq.client.util;

import java.util.List;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.shuimutong.gmq.client.bean.SendMqResult;
import com.shuimutong.gmq.client.cache.CommonObjCache;
import com.shuimutong.gmq.client.cache.bean.CacheObj;
import com.shuimutong.gmq.client.consumer.GmqConsumer;
import com.shuimutong.gmq.client.consumer.bean.MqContent;
import com.shuimutong.gmq.client.exception.ObjCacheException;
import com.shuimutong.gmq.client.exception.SendMqException;
import com.shuimutong.gmq.client.producer.GmqProducer;

public class GmqInstanceManageTest {
	String gmqServerUrl = "http://localhost:8080";
	String topic = "testa";
	
	@Test
	public void produceMsg() {
		GmqProducer producer = GmqInstanceManage.getGmqProducer(gmqServerUrl);
		for(int i=0; i<5; i++) {
			String message = "message:" + i;
			try {
				SendMqResult res = producer.sendMq(topic, message);
				System.out.println(res.getRes());
			} catch (SendMqException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Test
	public void comsumerMsg() {
		GmqConsumer comsumer = GmqInstanceManage.getGmqConsumer(gmqServerUrl);
		int size = 2;
		for(int i=0; i<4; i++) {
			int offset = i * size;
			try {
				List<MqContent> res = comsumer.getMq(topic, offset, size);
				System.out.println(String.format("offset:%d,size:%d,res:%s",offset, size, JSONObject.toJSONString(res)));
			} catch (SendMqException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Test
	public void comsumerMsgByCache() {
		GmqConsumer comsumer = GmqInstanceManage.getGmqConsumer(gmqServerUrl);
		CommonObjCache commonCache = GmqInstanceManage.getCommonCache(gmqServerUrl);
		String gmqSign = "gmq_consumer_id";
		long consumerId = 0;
		int size = 2;
		for(int i=0; i<5; i++) {
			try {
				CacheObj cacheId = commonCache.getById(gmqSign);
				if(cacheId != null) {
					consumerId = Long.parseLong(cacheId.getContent());
				}
				
				List<MqContent> res = comsumer.getMq(topic, consumerId, size);
				for(MqContent mq : res) {
					System.out.println(JSONObject.toJSONString(mq));
					if(mq.getId() > consumerId) {
						consumerId = mq.getId();
					}
				}
				commonCache.save(gmqSign, String.valueOf(consumerId));
				System.out.println("保存consumerId:" + consumerId);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Test
	public void cache() {
		CommonObjCache commonCache = GmqInstanceManage.getCommonCache(gmqServerUrl);
		String key = "test";
		try {
			CacheObj saved = commonCache.getById(key);
			System.out.println("before------");
			System.out.println(JSONObject.toJSONString(saved));
			String content = "Hello,cache!";
			commonCache.save(key, content);
			saved = commonCache.getById(key);
			System.out.println("after------");
			System.out.println(JSONObject.toJSONString(saved));
			commonCache.delById(key);
			saved = commonCache.getById(key);
			System.out.println("after-del------");
			System.out.println(JSONObject.toJSONString(saved));
		} catch (ObjCacheException e) {
			e.printStackTrace();
		}
	}
}
