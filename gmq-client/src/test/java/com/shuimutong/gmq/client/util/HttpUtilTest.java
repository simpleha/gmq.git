package com.shuimutong.gmq.client.util;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.shuimutong.gmq.client.bean.HttpResponseBean;

public class HttpUtilTest {
	@Test
	public void get() {
		String url = "http://www.baidu.com";
		try {
			HttpResponseBean res = HttpUtil.get(url);
			System.out.println(res);
			Assert.assertTrue(res.getStateCode() > 0);
			Assert.assertTrue(StringUtils.isNotBlank(res.getBody()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
