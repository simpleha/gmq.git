package com.shuimutong.gmq.server.service;

import java.util.List;

import com.shuimutong.gmq.server.bean.dos.ObjCacheDo;
import com.shuimutong.gmq.server.exception.DaoException;
import com.shuimutong.gmq.server.exception.ServiceException;

public interface ObjCacheService {
	/**
	 * 新增
	 * @param objId
	 * @param parentObjId
	 * @param content
	 * @throws ServiceException
	 */
	void add(String objId, String parentObjId, String content) throws ServiceException;
	
	/**
	 * 新增
	 * @param objId
	 * @param parentObjId
	 * @param content
	 * @param expireMs
	 * @throws ServiceException
	 */
	void add(String objId, String parentObjId, String content, long expireMs) throws ServiceException;

	/**
	 * 更新或者新增
	 * @param objId
	 * @param content
	 * @throws ServiceException
	 */
	void addOrUpdate(String objId, String content) throws ServiceException;

	/**
	 * 根据父id查询列表
	 * @param parentObjId
	 * @return
	 * @throws ServiceException
	 */
	List<ObjCacheDo> findByParentObjId(String parentObjId) throws ServiceException;

	/**
	 * 根据id查询
	 * @param objId
	 * @return
	 * @throws ServiceException
	 */
	ObjCacheDo findByObjId(String objId) throws ServiceException;
	
	/**
	 * 根据objId删除记录
	 * @param objId
	 * @throws ServiceException
	 */
	void delByObjId(String objId) throws ServiceException;
}
