package com.shuimutong.gmq.server.dao;

import com.shuimutong.gmq.server.bean.dos.TopicDo;
import com.shuimutong.gmq.server.exception.DaoException;

/**
 * 主题dao
 * @ClassName:  TopicDao   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年10月22日 下午9:06:24     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
public interface TopicDao {

	/**
	 * 根据id删除
	 * @param id
	 * @throws DaoException
	 */
	void delById(long id) throws DaoException;

	/**
	 * 通过id查找
	 * @param id
	 * @return
	 * @throws DaoException
	 */
	TopicDo findById(long id) throws DaoException;

	/**
	 * 添加主题
	 * @param topic
	 * @throws DaoException
	 */
	void addTopic(String topic) throws DaoException;

	/**
	 * 根据topic查找
	 * @param topic
	 * @return
	 * @throws DaoException
	 */
	TopicDo findByTopic(String topic) throws DaoException;

}
