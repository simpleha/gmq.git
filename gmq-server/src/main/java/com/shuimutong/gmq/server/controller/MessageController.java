package com.shuimutong.gmq.server.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.shuimutong.gmq.server.bean.SystemConstant;
import com.shuimutong.gmq.server.bean.dos.TopicDo;
import com.shuimutong.gmq.server.bean.enums.RequestParamEnum;
import com.shuimutong.gmq.server.bean.enums.ResponseCodeEnum;
import com.shuimutong.gmq.server.bean.vo.ResponseDataVo;
import com.shuimutong.gmq.server.exception.ServiceException;
import com.shuimutong.gmq.server.service.MessageService;
import com.shuimutong.gmq.server.service.TopicService;
import com.shuimutong.gmvc.annotation.XAutowired;
import com.shuimutong.gmvc.annotation.XController;
import com.shuimutong.gmvc.annotation.XRequestMapping;
import com.shuimutong.gmvc.util.RequestResolveUtil;
import com.shuimutong.guti.bean.TwoTuple;

/**
 * 发消息、收消息controller
 * @ClassName:  MessageController   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年10月20日 下午9:45:47     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
@XController
@XRequestMapping(SystemConstant.STR_URL_MESSAGE)
public class MessageController {
	private final static Logger log = LoggerFactory.getLogger(MessageController.class);
	@XAutowired
	private MessageService messageService;
	@XAutowired
	private TopicService topicService;
	

	/**
	 * 获取消息
	 * @param request
	 * @param reponse
	 */
    @XRequestMapping(SystemConstant.STR_URL_GET_MESSAGE)
    public void getMessage(HttpServletRequest request, HttpServletResponse reponse) {
    	ResponseDataVo responseData = null;
    	String topic = request.getParameter(RequestParamEnum.TOPIC.getParamName());
    	String offsetStr = request.getParameter(RequestParamEnum.OFFSET.getParamName());
    	String sizeStr = request.getParameter(RequestParamEnum.SIZE.getParamName());
    	
    	if(StringUtils.isBlank(topic) || !StringUtils.isNumeric(offsetStr) || !StringUtils.isNumeric(sizeStr)) {
    		responseData = new ResponseDataVo(ResponseCodeEnum.PARAM_ERROR, "主题为空或者数字非法");
    	} else {
    		int offset = Integer.parseInt(offsetStr);
    		int size = Integer.parseInt(sizeStr);
    		if(offset < 0 || size < 1) {
    			responseData = new ResponseDataVo(ResponseCodeEnum.PARAM_ERROR, "数字异常");
    		} else {
    			try {
    				TopicDo topicDo = topicService.findByTopic(topic);
    				if(topicDo == null) {
    					responseData = new ResponseDataVo(ResponseCodeEnum.PARAM_ERROR, "主题不存在");
    				} else {
    					List<TwoTuple<Long, String>> list = messageService.listMessage(topic, offset, size);
    					responseData = new ResponseDataVo(ResponseCodeEnum.OK, list);
    				}
				} catch (ServiceException e) {
					log.error("getMessageException", e);
					responseData = new ResponseDataVo(ResponseCodeEnum.SERVER_ERROR);
				}
    		}
    	}
    	RequestResolveUtil.returnJson(request, reponse, JSONObject.toJSONString(responseData));
    }
    
    
    /**
     * 生产方发送消息到服务端
     * @param request
     * @param reponse
     */
    @XRequestMapping(SystemConstant.STR_URL_SEND_MESSAGE)
    public void sendMessage(HttpServletRequest request, HttpServletResponse reponse) {
    	ResponseDataVo responseData = null;
    	String topic = request.getParameter(RequestParamEnum.TOPIC.getParamName());
    	String message = request.getParameter(RequestParamEnum.MESSAGE.getParamName());
    	if(StringUtils.isBlank(topic) || StringUtils.isBlank(message)) {
    		responseData = new ResponseDataVo(ResponseCodeEnum.PARAM_ERROR, "主题或者消息为空");
    	} else {
    		try {
				TopicDo topicDo = topicService.findByTopic(topic);
				if(topicDo == null) {
					responseData = new ResponseDataVo(ResponseCodeEnum.PARAM_ERROR, "主题不存在");
				} else {
					messageService.saveMessage(topic, message);
					responseData = new ResponseDataVo(ResponseCodeEnum.OK);
				}
			} catch (ServiceException e) {
				log.error("sendMessageException", e);
				responseData = new ResponseDataVo(ResponseCodeEnum.SERVER_ERROR);
			}
    	}
    	RequestResolveUtil.returnJson(request, reponse, JSONObject.toJSONString(responseData));
    }
}