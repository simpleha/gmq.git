package com.shuimutong.gmq.server.bean.vo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.shuimutong.gmq.server.bean.enums.RequestParamEnum;

/**
 * uri描述vo
 * @ClassName:  UriDescVo   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年10月27日 上午10:56:55     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
public class UriDescVo {
	/**uri**/
	private String uri;
	/**功能描述**/
	private String desc;
	/**参数列表<参数名, 说明>**/
	private Map<String, String> params = new HashMap();
	
	public UriDescVo(String uri, String desc, List<RequestParamEnum> paramList) {
		this.uri = uri;
		this.desc = desc;
		for(RequestParamEnum parm : paramList) {
			this.params.put(parm.getParamName(), parm.getDesc());
		}
	}
	public UriDescVo() {
		super();
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Map<String, String> getParams() {
		return params;
	}
	public void setParams(Map<String, String> params) {
		this.params = params;
	}
	
	
}
