package com.shuimutong.gmq.server.service.impl;

import java.util.List;

import com.shuimutong.gmq.server.bean.SystemConstant;
import com.shuimutong.gmq.server.bean.vo.UriDescVo;
import com.shuimutong.gmq.server.service.SyncService;
import com.shuimutong.gmvc.annotation.XService;

@XService
public class SyncServiceImpl implements SyncService {
	@Override
	public List<UriDescVo> listUriDesc() {
		return SystemConstant.MESSAGE_URI_LIST;
	}
}
