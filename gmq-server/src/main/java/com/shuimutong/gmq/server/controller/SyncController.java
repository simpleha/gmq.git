package com.shuimutong.gmq.server.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.shuimutong.gmq.server.bean.enums.RequestParamEnum;
import com.shuimutong.gmq.server.bean.enums.ResponseCodeEnum;
import com.shuimutong.gmq.server.bean.vo.ResponseDataVo;
import com.shuimutong.gmq.server.bean.vo.UriDescVo;
import com.shuimutong.gmq.server.exception.ServiceException;
import com.shuimutong.gmq.server.service.SyncService;
import com.shuimutong.gmq.server.service.TopicService;
import com.shuimutong.gmvc.annotation.XAutowired;
import com.shuimutong.gmvc.annotation.XController;
import com.shuimutong.gmvc.annotation.XRequestMapping;
import com.shuimutong.gmvc.util.RequestResolveUtil;

/**
 * 非消息信息同步controller
 * @ClassName:  MessageController   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年10月20日 下午9:45:47     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
@XController
@XRequestMapping("/sync")
public class SyncController {
	private final static Logger log = LoggerFactory.getLogger(SyncController.class);
	@XAutowired
	private TopicService topicService;
	@XAutowired
	private SyncService syncService;

	/**
	 * 获取uri说明
	 * @param request
	 * @param reponse
	 */
    @XRequestMapping("/getPath")
    public void getPath(HttpServletRequest request, HttpServletResponse reponse) {
    	List<UriDescVo> uriList = syncService.listUriDesc();
    	ResponseDataVo responseData = new ResponseDataVo(ResponseCodeEnum.OK, uriList);
    	RequestResolveUtil.returnJson(request, reponse, JSONObject.toJSONString(responseData));
    }
    
    /**
     * 新增topic
     * @param request
     * @param reponse
     */
    @XRequestMapping("/addTopic")
    public void addTopic(HttpServletRequest request, HttpServletResponse reponse) {
    	ResponseDataVo responseData = null;
    	String topic = request.getParameter(RequestParamEnum.TOPIC.getParamName());
    	if(StringUtils.isBlank(topic)) {
    		responseData = new ResponseDataVo(ResponseCodeEnum.PARAM_ERROR, "主题为空");
    	} else {
    		try {
    			boolean addState = topicService.addTopic(topic);
    			if(addState) {
    				responseData = new ResponseDataVo(ResponseCodeEnum.OK, "添加成功");
    			} else {
    				responseData = new ResponseDataVo(ResponseCodeEnum.PARAM_ERROR, "主题已存在");
    			}
			} catch (ServiceException e) {
				log.error("addTopicException," + topic, e);
				responseData = new ResponseDataVo(ResponseCodeEnum.SERVER_ERROR);
			}
    	}
    	RequestResolveUtil.returnJson(request, reponse, JSONObject.toJSONString(responseData));
    }
}