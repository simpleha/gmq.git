package com.shuimutong.gmq.server.bean.dos;

import java.util.Date;

import me.lovegao.gdao.bean.annotation.GColumn;
import me.lovegao.gdao.bean.annotation.GId;
import me.lovegao.gdao.bean.annotation.GTable;

@GTable("gmq_topic")
public class TopicDo {
	@GId(isAutoIncrease=true)
    @GColumn(name="id")
    private long id;
    
    @GColumn(name="topic")
    private String topic;
    
    @GColumn(name="create_time")
    private Date createTime;

    
	public TopicDo(String topic) {
		this.topic = topic;
		this.createTime = new Date();
	}

	public TopicDo() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
