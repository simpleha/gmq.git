package com.shuimutong.gmq.server.service.impl;

import org.apache.commons.lang3.StringUtils;

import com.shuimutong.gmq.server.bean.dos.TopicDo;
import com.shuimutong.gmq.server.dao.TopicDao;
import com.shuimutong.gmq.server.exception.DaoException;
import com.shuimutong.gmq.server.exception.ServiceException;
import com.shuimutong.gmq.server.service.TopicService;
import com.shuimutong.gmvc.annotation.XAutowired;
import com.shuimutong.gmvc.annotation.XService;

@XService
public class TopicServiceImpl implements TopicService {
	@XAutowired
	private TopicDao topicDao;
	
	@Override
	public boolean addTopic(String topic) throws ServiceException {
		boolean add = false;
		if(StringUtils.isNotBlank(topic)) {
			try {
				TopicDo topicDo = topicDao.findByTopic(topic);
				if(topicDo == null) {
					topicDao.addTopic(topic);
					add = true;
				}
			} catch (DaoException e) {
				throw new ServiceException(e);
			}
		}
		return add;
	}
	
	@Override
	public TopicDo findByTopic(String topic) throws ServiceException {
		TopicDo topicDo = null;
		if(StringUtils.isNotBlank(topic)) {
			try {
				topicDo = topicDao.findByTopic(topic);
			} catch (DaoException e) {
				throw new ServiceException(e);
			}
		}
		return topicDo;
	}
	
	@Override
	public void delTopic(String topic) throws ServiceException {
		if(StringUtils.isNotBlank(topic)) {
			try {
				TopicDo topicDo = topicDao.findByTopic(topic);
				if(topicDo != null) {
					topicDao.delById(topicDo.getId());
				}
			} catch (DaoException e) {
				throw new ServiceException(e);
			}
		}
	}
}
