package com.shuimutong.gmq.server.init;

import java.util.Properties;

import com.shuimutong.gmvc.annotation.XServerInit;
import com.shuimutong.gmvc.bean.ServerInit;

import me.lovegao.gdao.instancemanager.DaoResourceManager;
import me.lovegao.gdao.sqlexecute.ISqlExecutor;

@XServerInit(priority=1)
public class GmqServerInit implements ServerInit {
	private static ISqlExecutor SQL_EXECUTOR;

	@Override
	public void init() throws Exception {
		SQL_EXECUTOR = initAndGetSqlExecutor();
	}
	
	public static ISqlExecutor getSqlExecutor() {
		return SQL_EXECUTOR;
	}

	private static ISqlExecutor initAndGetSqlExecutor() throws Exception {
		Properties prop = new Properties();
		String confPath = "/mysql.properties";
		prop.load(GmqServerInit.class.getResourceAsStream(confPath));
		DaoResourceManager daoResource = new DaoResourceManager(prop);
		ISqlExecutor sqlExecutor = daoResource.getSqlExecutor();
		return sqlExecutor;
	}
}
