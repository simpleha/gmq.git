package com.shuimutong.gmq.server.bean;

import java.util.ArrayList;
import java.util.List;

import com.shuimutong.gmq.server.bean.enums.RequestParamEnum;
import com.shuimutong.gmq.server.bean.vo.UriDescVo;

/**
 * 系统常量
 * @ClassName:  SystemConstant   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年10月20日 下午9:42:44     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
public class SystemConstant {
	/**字符串-消息的根uri**/
	public final static String STR_URL_MESSAGE = "/message";
	/**字符串-获取消息的uri**/
	public final static String STR_URL_GET_MESSAGE = "/get";
	/**字符串-发送消息的uri**/
	public final static String STR_URL_SEND_MESSAGE = "/send";
	
	/**收发消息的uri说明**/
	public final static List<UriDescVo> MESSAGE_URI_LIST = new ArrayList();
	
	static {
		//初始化消息的uri列表
		String uri1 = SystemConstant.STR_URL_MESSAGE + SystemConstant.STR_URL_GET_MESSAGE;
		List<RequestParamEnum> paramList1 = new ArrayList();
		paramList1.add(RequestParamEnum.TOPIC);
		paramList1.add(RequestParamEnum.OFFSET);
		paramList1.add(RequestParamEnum.SIZE);
		MESSAGE_URI_LIST.add(new UriDescVo(uri1, "获取消息",paramList1));
		
		String uri2 = SystemConstant.STR_URL_MESSAGE + SystemConstant.STR_URL_SEND_MESSAGE;
		List<RequestParamEnum> paramList2 = new ArrayList();
		paramList2.add(RequestParamEnum.TOPIC);
		paramList2.add(RequestParamEnum.MESSAGE);
		MESSAGE_URI_LIST.add(new UriDescVo(uri2, "发送消息",paramList2));
		
	}
}
