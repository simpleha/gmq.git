package com.shuimutong.gmq.server.dao;

import java.util.List;

import com.shuimutong.gmq.server.exception.DaoException;
import com.shuimutong.guti.bean.TwoTuple;

/**
 * 消息存储dao
 * @ClassName:  MessageDao   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年10月22日 下午10:00:35     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
public interface MessageDao {

	/**
	 * 获取消息
	 * @param topic 主题
	 * @param offset 消息编号
	 * @param size 获取消息数量
	 * @return
	 * @throws DaoException
	 */
	List<TwoTuple<Long, String>> listMessage(String topic, long offset, int size) throws DaoException;

	/**
	 * 保存消息
	 * @param topic 主题
	 * @param body 消息内容
	 * @throws DaoException
	 */
	void saveMessage(String topic, String body) throws DaoException;

}
