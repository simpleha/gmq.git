package com.shuimutong.gmq.server.bean.enums;

/**
 * 请求参数枚举
 * @ClassName:  RequestParamEnum   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年10月27日 上午10:44:10     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
public enum RequestParamEnum {
	TOPIC("topic", "正常"),
	MESSAGE("message", "消息"),
	OFFSET("offset", "offset"),
	SIZE("size", "size"),;
	
	RequestParamEnum(String paramName, String desc) {
		this.paramName = paramName;
		this.desc = desc;
	}
	
	/**字段名**/
	private String paramName;
	/**字段描述**/
	private String desc;
	
	public String getDesc() {
		return desc;
	}
	public String getParamName() {
		return paramName;
	}
}
