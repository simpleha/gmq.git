package com.shuimutong.gmq.server.service;

import com.shuimutong.gmq.server.bean.dos.TopicDo;
import com.shuimutong.gmq.server.exception.ServiceException;

/**
 * 主题service
 * @ClassName:  TopicService   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年10月24日 下午9:28:21     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
public interface TopicService {

	/**
	 * 删除主题
	 * @param topic
	 * @throws ServiceException
	 */
	void delTopic(String topic) throws ServiceException;

	/**
	 * 查找主题
	 * @param topic
	 * @return
	 * @throws ServiceException
	 */
	TopicDo findByTopic(String topic) throws ServiceException;

	/**
	 * 添加主题
	 * @param topic
	 * @throws ServiceException
	 */
	boolean addTopic(String topic) throws ServiceException;
	
}
