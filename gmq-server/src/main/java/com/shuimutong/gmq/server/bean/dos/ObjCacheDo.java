package com.shuimutong.gmq.server.bean.dos;

import me.lovegao.gdao.bean.annotation.GColumn;
import me.lovegao.gdao.bean.annotation.GId;
import me.lovegao.gdao.bean.annotation.GTable;

@GTable("obj_cache")
public class ObjCacheDo {
	@GId(isAutoIncrease=true)
    @GColumn(name="id")
    private long id;
    
	/**对象id**/
    @GColumn(name="obj_id")
    private String objId;
    
    /**父id**/
    @GColumn(name="parent_obj_id")
    private String parentObjId;
    
    /**内容**/
    @GColumn(name="content")
    private String content;
    
    /**创建时间-毫秒**/
    @GColumn(name="create_time")
    private long createTime;
    
    /**过期时间-毫秒**/
    @GColumn(name="expire_ms")
    private long expireMs = -1;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getObjId() {
		return objId;
	}

	public void setObjId(String objId) {
		this.objId = objId;
	}


	public String getParentObjId() {
		return parentObjId;
	}

	public void setParentObjId(String parentObjId) {
		this.parentObjId = parentObjId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public long getExpireMs() {
		return expireMs;
	}

	public void setExpireMs(long expireMs) {
		this.expireMs = expireMs;
	}
    
    
}
