package com.shuimutong.gmq.server.dao.impl;

import java.io.Serializable;

import com.shuimutong.gmq.server.init.GmqServerInit;

import me.lovegao.gdao.orm.AbstractBaseDao;
import me.lovegao.gdao.sqlexecute.ISqlExecutor;

public class BaseDao<T, PK extends Serializable> extends AbstractBaseDao<T, PK> {

	@Override
	public ISqlExecutor getSqlExecutor() {
		return GmqServerInit.getSqlExecutor();
	}

}
