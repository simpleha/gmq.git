package com.shuimutong.gmq.server.service;

import java.util.List;

import com.shuimutong.gmq.server.exception.ServiceException;
import com.shuimutong.guti.bean.TwoTuple;

/**
 * 消息service
 * @ClassName:  MessageService   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年10月24日 下午9:36:17     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
public interface MessageService {

	/**
	 * 获取消息
	 * @param topic
	 * @param offset
	 * @param size
	 * @return
	 * @throws ServiceException
	 */
	List<TwoTuple<Long, String>> listMessage(String topic, long offset, int size) throws ServiceException;

	/**
	 * 保存消息
	 * @param topic
	 * @param body
	 * @throws ServiceException
	 */
	void saveMessage(String topic, String body) throws ServiceException;

}
