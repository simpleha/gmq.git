package com.shuimutong.gmq.server.bean.dos;

import java.util.Date;

import me.lovegao.gdao.bean.annotation.GColumn;
import me.lovegao.gdao.bean.annotation.GId;
import me.lovegao.gdao.bean.annotation.GTable;

@GTable("gmq_message")
public class MessageDo {
	@GId(isAutoIncrease=true)
    @GColumn(name="id")
    private long id;
    
    @GColumn(name="topic")
    private String topic;

    @GColumn(name="message_body")
    private String messageBody;

    @GColumn(name="create_time")
    private Date createTime;
    
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
    
}
