package com.shuimutong.gmq.server.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.shuimutong.gmq.server.dao.MessageDao;
import com.shuimutong.gmq.server.exception.DaoException;
import com.shuimutong.gmq.server.exception.ServiceException;
import com.shuimutong.gmq.server.service.MessageService;
import com.shuimutong.gmvc.annotation.XAutowired;
import com.shuimutong.gmvc.annotation.XService;
import com.shuimutong.guti.bean.TwoTuple;

@XService
public class MessageServiceImpl implements MessageService {
	@XAutowired
	private MessageDao messageDao;
	
	@Override
	public void saveMessage(String topic, String body) throws ServiceException {
		if(StringUtils.isNotBlank(topic) && StringUtils.isNotBlank(body)) {
			try {
				messageDao.saveMessage(topic, body);
			} catch (DaoException e) {
				throw new ServiceException(e);
			}
		}
	}
	
	@Override
	public List<TwoTuple<Long, String>> listMessage(String topic, long offset, int size) throws ServiceException {
		List<TwoTuple<Long, String>> list = null;
		if(StringUtils.isNotBlank(topic) && offset >= 0 && size > 0) {
			try {
				list = messageDao.listMessage(topic, offset, size);
			} catch (DaoException e) {
				throw new ServiceException(e);
			}
		}
		return list;
	}
}
