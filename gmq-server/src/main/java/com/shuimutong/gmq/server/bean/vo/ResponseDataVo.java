package com.shuimutong.gmq.server.bean.vo;

import com.shuimutong.gmq.server.bean.enums.ResponseCodeEnum;

/**
 * 响应数据vo
 * @ClassName:  ResponseDataVo   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年10月24日 下午9:39:51     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
public class ResponseDataVo {
	/**状态**/
	private int state;
	/**状态描述**/
	private String desc;
	/**详细描述**/
	private String msg;
	/**数据**/
	private Object data;
	
	public ResponseDataVo(ResponseCodeEnum responseCode) {
		this.state = responseCode.getCode();
		this.desc = responseCode.getDesc();
	}
	public ResponseDataVo(ResponseCodeEnum responseCode, Object data) {
		this.state = responseCode.getCode();
		this.desc = responseCode.getDesc();
		this.data = data;
	}
	public ResponseDataVo(ResponseCodeEnum responseCode, String msg) {
		this.state = responseCode.getCode();
		this.desc = responseCode.getDesc();
		this.msg = msg;
	}
	
	
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}
