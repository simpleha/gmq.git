package com.shuimutong.gmq.server.exception;

/**
 * service层异常
 * @ClassName:  ServiceException   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年10月22日 下午8:58:22     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
public class ServiceException extends Exception {

	public ServiceException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ServiceException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ServiceException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
