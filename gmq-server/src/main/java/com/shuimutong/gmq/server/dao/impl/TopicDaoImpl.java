package com.shuimutong.gmq.server.dao.impl;

import java.util.List;

import com.shuimutong.gmq.server.bean.dos.TopicDo;
import com.shuimutong.gmq.server.dao.TopicDao;
import com.shuimutong.gmq.server.exception.DaoException;
import com.shuimutong.gmvc.annotation.XRepository;
import com.shuimutong.gutil.common.GUtilCommonUtil;

@XRepository
public class TopicDaoImpl extends BaseDao<TopicDo, Long> implements TopicDao {
	
	@Override
	public void addTopic(String topic) throws DaoException {
		TopicDo entity = new TopicDo(topic);
		try {
			super.add(entity);
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}
	
	@Override
	public TopicDo findById(long id) throws DaoException {
		TopicDo entity = null;
		try {
			entity = super.queryByPK(id);
		} catch (Exception e) {
			throw new DaoException(e);
		}
		return entity;
	}
	
	@Override
	public TopicDo findByTopic(String topic) throws DaoException {
		TopicDo entity = null;
		try {
			String sql = "select id, topic, create_time from gmq_topic where topic=?";
			List<TopicDo> list = super.list(sql, topic);
			if(!GUtilCommonUtil.checkListEmpty(list)) {
				entity = list.get(0);
			}
		} catch (Exception e) {
			throw new DaoException(e);
		}
		return entity;
	}
	
	@Override
	public void delById(long id) throws DaoException {
		try {
			super.deleteByPK(id);
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

}
