package com.shuimutong.gmq.server.service;

import java.util.List;

import com.shuimutong.gmq.server.bean.vo.UriDescVo;

/**
 * 其他信息同步service
 * @ClassName:  SyncService   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年10月27日 上午11:05:33     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
public interface SyncService {
	/**
	 * 获取uri说明
	 * @return
	 */
	List<UriDescVo> listUriDesc();

}
