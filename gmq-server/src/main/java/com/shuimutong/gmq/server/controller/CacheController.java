package com.shuimutong.gmq.server.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.shuimutong.gmq.server.bean.dos.ObjCacheDo;
import com.shuimutong.gmq.server.bean.enums.ResponseCodeEnum;
import com.shuimutong.gmq.server.bean.vo.ResponseDataVo;
import com.shuimutong.gmq.server.exception.ServiceException;
import com.shuimutong.gmq.server.service.ObjCacheService;
import com.shuimutong.gmvc.annotation.XAutowired;
import com.shuimutong.gmvc.annotation.XController;
import com.shuimutong.gmvc.annotation.XRequestMapping;
import com.shuimutong.gmvc.util.RequestResolveUtil;
import com.shuimutong.gutil.common.GUtilCommonUtil;

/**
 * 缓存controller
 * @ClassName:  CacheController   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年10月27日 下午3:48:38     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
@XController
@XRequestMapping("/cache")
public class CacheController {
	private final static Logger log = LoggerFactory.getLogger(CacheController.class);
	@XAutowired
	private ObjCacheService objCacheService;

	/**
	 * 添加有过期时间的记录
	 * @param request
	 * @param reponse
	 */
    @XRequestMapping("/setWithExpire")
    public void setWithExpire(HttpServletRequest request, HttpServletResponse reponse) {
    	ResponseDataVo responseData = null;
    	String objId = request.getParameter("objId");
    	String parentObjId = request.getParameter("parentObjId");
    	String content = request.getParameter("content");
    	String expireMs = request.getParameter("expireMs");
    	if(StringUtils.isBlank(objId) || !StringUtils.isNumeric(expireMs)) {
    		responseData = new ResponseDataVo(ResponseCodeEnum.PARAM_ERROR, "objId为空或者过期毫秒数非法");
    	} else {
    		long expireMsLong = -1;
			try {
				expireMsLong = Long.parseLong(expireMs);
			} catch (NumberFormatException e1) {
				responseData = new ResponseDataVo(ResponseCodeEnum.PARAM_ERROR, "过期毫秒数非数字");
			}
    		try {
				objCacheService.add(objId, parentObjId, content, expireMsLong);
				responseData = new ResponseDataVo(ResponseCodeEnum.OK);
			} catch (ServiceException e) {
				log.error("setWithExpireException", e);
				responseData = new ResponseDataVo(ResponseCodeEnum.PARAM_ERROR, "objId已存在，请检查");
			}
    	}
    	RequestResolveUtil.returnJson(request, reponse, JSONObject.toJSONString(responseData));
    }
    
    /**
     * 添加新记录
     * @param request
     * @param reponse
     */
    @XRequestMapping("/set")
    public void set(HttpServletRequest request, HttpServletResponse reponse) {
    	ResponseDataVo responseData = null;
    	String objId = request.getParameter("objId");
    	String parentObjId = request.getParameter("parentObjId");
    	String content = request.getParameter("content");
    	if(StringUtils.isBlank(objId)) {
    		responseData = new ResponseDataVo(ResponseCodeEnum.PARAM_ERROR, "objId为空");
    	} else {
    		try {
    			objCacheService.add(objId, parentObjId, content);
    			responseData = new ResponseDataVo(ResponseCodeEnum.OK);
    		} catch (ServiceException e) {
				log.error("setException", e);
    			responseData = new ResponseDataVo(ResponseCodeEnum.PARAM_ERROR, "objId已存在，请检查");
    		}
    	}
    	RequestResolveUtil.returnJson(request, reponse, JSONObject.toJSONString(responseData));
    }
    
    /**
     * 保存新记录或者更新记录
     * @param request
     * @param reponse
     */
    @XRequestMapping("/saveOrUpdate")
    public void saveOrUpdate(HttpServletRequest request, HttpServletResponse reponse) {
    	ResponseDataVo responseData = null;
    	String objId = request.getParameter("objId");
    	String content = request.getParameter("content");
    	if(StringUtils.isBlank(objId)) {
    		responseData = new ResponseDataVo(ResponseCodeEnum.PARAM_ERROR, "objId为空");
    	} else {
    		try {
    			objCacheService.addOrUpdate(objId, content);
    			responseData = new ResponseDataVo(ResponseCodeEnum.OK);
    		} catch (ServiceException e) {
				log.error("saveOrUpdateException", e);
    			responseData = new ResponseDataVo(ResponseCodeEnum.SERVER_ERROR, "服务异常，请重试");
    		}
    	}
    	RequestResolveUtil.returnJson(request, reponse, JSONObject.toJSONString(responseData));
    }
    
    /**
     * 根据objId查找记录
     * @param request
     * @param reponse
     */
    @XRequestMapping("/findByObjId")
    public void findByObjId(HttpServletRequest request, HttpServletResponse reponse) {
    	ResponseDataVo responseData = null;
    	String objId = request.getParameter("objId");
    	if(StringUtils.isBlank(objId)) {
    		responseData = new ResponseDataVo(ResponseCodeEnum.PARAM_ERROR, "objId为空");
    	} else {
    		try {
    			ObjCacheDo objCache = objCacheService.findByObjId(objId);
    			responseData = new ResponseDataVo(ResponseCodeEnum.OK, objCache);
    		} catch (ServiceException e) {
				log.error("findByParentObjIdException", e);
    			responseData = new ResponseDataVo(ResponseCodeEnum.SERVER_ERROR, "服务异常，请重试");
    		}
    	}
    	RequestResolveUtil.returnJson(request, reponse, JSONObject.toJSONString(responseData));
    }
    
    /**
     * 根据父objId查找记录列表
     * @param request
     * @param reponse
     */
    @XRequestMapping("/findByParentObjId")
    public void findByParentObjId(HttpServletRequest request, HttpServletResponse reponse) {
    	ResponseDataVo responseData = null;
    	String parentObjId = request.getParameter("parentObjId");
    	if(StringUtils.isBlank(parentObjId)) {
    		responseData = new ResponseDataVo(ResponseCodeEnum.PARAM_ERROR, "parentObjId为空");
    	} else {
    		try {
    			List<ObjCacheDo> objCacheList = objCacheService.findByParentObjId(parentObjId);
    			responseData = new ResponseDataVo(ResponseCodeEnum.OK, objCacheList);
    		} catch (ServiceException e) {
				log.error("findByParentObjIdException", e);
    			responseData = new ResponseDataVo(ResponseCodeEnum.SERVER_ERROR, "服务异常，请重试");
    		}
    	}
    	RequestResolveUtil.returnJson(request, reponse, JSONObject.toJSONString(responseData));
    }
    
    /**
     * 根据objId删除记录
     * @param request
     * @param reponse
     */
    @XRequestMapping("/delByObjId")
    public void delByObjId(HttpServletRequest request, HttpServletResponse reponse) {
    	ResponseDataVo responseData = null;
    	String objId = request.getParameter("objId");
    	if(StringUtils.isBlank(objId)) {
    		responseData = new ResponseDataVo(ResponseCodeEnum.PARAM_ERROR, "objId为空");
    	} else {
    		try {
    			objCacheService.delByObjId(objId);
    			responseData = new ResponseDataVo(ResponseCodeEnum.OK);
    		} catch (ServiceException e) {
				log.error("delByObjIdException", e);
    			responseData = new ResponseDataVo(ResponseCodeEnum.SERVER_ERROR, "服务异常，请重试");
    		}
    	}
    	RequestResolveUtil.returnJson(request, reponse, JSONObject.toJSONString(responseData));
    }
}