package com.shuimutong.gmq.server.exception;

/**
 * dao层异常
 * @ClassName:  DaoException   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年10月22日 下午8:57:45     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
public class DaoException extends Exception {

	public DaoException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DaoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DaoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DaoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
