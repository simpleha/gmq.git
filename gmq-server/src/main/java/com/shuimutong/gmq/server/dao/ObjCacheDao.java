package com.shuimutong.gmq.server.dao;

import java.util.List;

import com.shuimutong.gmq.server.bean.dos.ObjCacheDo;
import com.shuimutong.gmq.server.exception.DaoException;

/**
 * 缓存dao
 * @ClassName:  TopicDao   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年10月22日 下午9:06:24     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
public interface ObjCacheDao {

	/**
	 * 新增
	 * @param objId
	 * @param parentObjId
	 * @param content
	 * @throws DaoException
	 */
	void add(String objId, String parentObjId, String content) throws DaoException;
	
	/**
	 * 新增
	 * @param objId
	 * @param parentObjId
	 * @param content
	 * @param expireMs
	 * @throws DaoException
	 */
	void add(String objId, String parentObjId, String content, long expireMs) throws DaoException;

	/**
	 * 更新或者新增
	 * @param objId
	 * @param content
	 * @throws DaoException
	 */
	void addOrUpdate(String objId, String content) throws DaoException;

	/**
	 * 根据父id查询列表
	 * @param parentObjId
	 * @return
	 * @throws DaoException
	 */
	List<ObjCacheDo> findByParentObjId(String parentObjId) throws DaoException;

	/**
	 * 根据id查询
	 * @param objId
	 * @return
	 * @throws DaoException
	 */
	ObjCacheDo findByObjId(String objId) throws DaoException;

	/**
	 * 根据objId删除记录
	 * @param objId
	 * @throws DaoException
	 */
	void delByObjId(String objId) throws DaoException;


}
