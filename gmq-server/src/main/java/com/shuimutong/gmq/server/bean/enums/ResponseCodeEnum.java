package com.shuimutong.gmq.server.bean.enums;

/**
 * 响应码枚举
 * @ClassName:  ResponseCodeEnum   
 * @Description:(这里用一句话描述这个类的作用)   
 * @author: 水木桶
 * @date:   2019年10月24日 下午9:46:59     
 * @Copyright: 2019 [水木桶]  All rights reserved.
 */
public enum ResponseCodeEnum {
	OK(200, "正常"),
	SERVER_ERROR(500, "服务异常"),
	PARAM_ERROR(400, "参数异常"),;
	
	ResponseCodeEnum(int code, String desc) {
		this.code = code;
		this.desc = desc;
	}
	
	/**状态码**/
	private int code;
	/**状态描述**/
	private String desc;
	
	public int getCode() {
		return code;
	}
	public String getDesc() {
		return desc;
	}
}
