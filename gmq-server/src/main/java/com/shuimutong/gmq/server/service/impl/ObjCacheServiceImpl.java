package com.shuimutong.gmq.server.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.shuimutong.gmq.server.bean.dos.ObjCacheDo;
import com.shuimutong.gmq.server.dao.ObjCacheDao;
import com.shuimutong.gmq.server.exception.DaoException;
import com.shuimutong.gmq.server.exception.ServiceException;
import com.shuimutong.gmq.server.service.ObjCacheService;
import com.shuimutong.gmvc.annotation.XAutowired;
import com.shuimutong.gmvc.annotation.XService;

@XService
public class ObjCacheServiceImpl implements ObjCacheService {
	@XAutowired
	private ObjCacheDao objCacheDao;

	@Override
	public void add(String objId, String parentObjId, String content) throws ServiceException {
		if(StringUtils.isBlank(objId)) {
			throw new ServiceException("objId是空");
		}
		try {
			objCacheDao.add(objId, parentObjId, content);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void add(String objId, String parentObjId, String content, long expireMs) throws ServiceException {
		if(StringUtils.isBlank(objId)) {
			throw new ServiceException("objId是空");
		}
		try {
			objCacheDao.add(objId, parentObjId, content, expireMs);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void addOrUpdate(String objId, String content) throws ServiceException {
		if(StringUtils.isBlank(objId)) {
			throw new ServiceException("objId是空");
		}
		try {
			objCacheDao.addOrUpdate(objId, content);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<ObjCacheDo> findByParentObjId(String parentObjId) throws ServiceException {
		List<ObjCacheDo> list = null;
		if(StringUtils.isBlank(parentObjId)) {
			throw new ServiceException("parentObjId是空");
		}
		try {
			list = objCacheDao.findByParentObjId(parentObjId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
		return list;
	}

	@Override
	public ObjCacheDo findByObjId(String objId) throws ServiceException {
		ObjCacheDo objCache = null;
		if(StringUtils.isBlank(objId)) {
			throw new ServiceException("parentObjId是空");
		}
		try {
			objCache = objCacheDao.findByObjId(objId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
		return objCache;
	}

	@Override
	public void delByObjId(String objId) throws ServiceException {
		if(StringUtils.isBlank(objId)) {
			throw new ServiceException("parentObjId是空");
		}
		try {
			objCacheDao.delByObjId(objId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
}
